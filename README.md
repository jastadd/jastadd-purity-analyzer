# JastAdd-Purity-Analyzer (JPA)
JastAdd-Purity-Analyzer (JPA) is a tool for analyzing JastAdd projects for functional purity.
The tool combines a special code generation for JastAdd with Purano, a functional purity analysis tool for Java.
The output is side effects warnings for the Java methods produced by JastAdd.

Purano was developed by Jiachen Yang, the original tool can be found here: https://github.com/farseerfc/purano. 
JPA would not of been possible without the incredible work by Jiachen, thank you for sharing your tool.
Purano is part of two academic papers:\
"Revealing Purity and Side Effects on Functionsfor Reusing Java Libraries"\
"Towards Purity-Guided Refactoring in Java"\
Make sure to check them out.

# Usage
Add your grammar files (*.jrag, *.jadd, *.ast) to the spec/ folder.
To start the tool simply run ant.
The tool will analyze every Java method in the project for side effects.
Every statement of a method is analyzed for side effects, this includes calls to other methods. 
Running the tool subsequently will not delete old output folders.
After running the tool the output will be in out/YYYY-MM-DD:HH:MM:SS, i.e., in a folder with the date and time of the execution. 

The folder contains a file named analysis-summary.txt which summarizes the analysis results. 
The summary has information about the used configuration and a list of methods that were found impure.
See the section "Analysis Summary" further down for additional information about the structure of this file.

The folder also contains all the analysed Java classes of the JastAdd project. 
These folders contain a HTML file named out.html with purity information for the methods of the corresponding class.
Optionally, these folder can contain two folders named "dot" and "text" if the class has been analysed with the extended analysis.

The dot folder contains DOT files (graphviz) for the methods in the class. 
These files can be used to visualize which program statements that the method can execute that causes side effects. This includes program statements of other methods that is part of the methods call graph.
The *.dot files can be used to generate a graph as, e.g., a pdf or png.
For example, a file "int_AST.A#x_().dot" can be used to generated a pdf file using the following command: dot -Tpdf int_AST.A#x_().dot > x.pdf\
This will convert the dot file to a pdf with the name x.pdf.

The text folder contains a .txt file for each method in the class. 
These files can be used to identify program statements that the method can execute that causes side effects.
The side effects warnings have two parts, first a method call chain showing how the program statement is reachable from
the analyzed method. If the statement is reachable from multiple call chains only one of them will shown. 
Second, the warning has information about the program statement that causes the side effect. This information includes
the line number of the statement, the name and type of the class field that is affected, the name of the class that owns the field.

# Extended Analysis
The tool will by default perform detailed purity analysis for all non external methods. 
This can, for large compilers such as JModelica or Extendj, cause the tool to produce large amounts of analyis data (several GBs). 
In these cases it is better to perform the detailed analysis for selected classes. 
To do this, change the "extended.analysis.for.all" property in build.xml to false, and manually add the classes you want detailed analysis for in extended-analysis.txt. 
For example, suppose you want detailed analysis for the methods in two classes named A and B, located in the package org.foo.bar, then your extended-analysis.txt should have the following content:\
org.foo.bar.A\
org.foo.bar.B

# Analysis Modes
The tool can be run in different modes: default, no_static, static, native. 
For example, in the no_static mode the tool will ignore side effects that affect static fields. This can be useful to
reduce the amount of output from the tool. In the static mode only side effects on static fields will be identified. The Native mode
will only identify calls to native methods (methods not running in the JVM). The default mode will identify all types of side effects but not calls to native methods. To change which mode the tool runs in change the value of the "analysis.mode" property in the build.xml file, found at the top of the file.

# Adding Source Folders and Packages
The tool supports adding external Java source code to be analyzed. This can be useful if, for example, you have a JastAdd based compiler with a complex build system. 
To analyse your compiler with the tool you can add the absolute paths of the folders with your Java generated code in the sourcedirs.txt file. Every source folder should be on a separate line in the file. 
Suppose that you are using linux, with the user name "usrname", and that you want to add two source folders named foo and bar located in a directory named mycompiler in your home folder, sourcedirs.txt should then have the following content:\
/home/usrname/mycompiler/foo\
/home/usrname/mycompiler/bar

Before you analyze your compiler with JPA you should build your Java code with the special version of JastAdd that JPA uses. 
The JPA version of JastAdd can be found in the jastadd2 directory of the root folder of this project, the file is named
jastadd2.jar.
To analyze your external compiler code you also have to add package names. If, for example, all your compiler code lives in
a package named org.extendj then this package name should be added to the packages.txt file. If your code is in
multiple packages then all of these have to be added to the packages.txt file, each package on a new line. For example,
if your code is located in the packages org.foo and org.bar then packages.txt should have the following content:\
org.foo\
org.bar

The tool generates output for source code in the sub packages of the packages that you add. Therefore, it is generally a good idea to avoid adding common high level package names such as "org" or "com". If you do, the tool will analyze large external libraries part of the Purano source code. For example, if all your code is contained in the package named org.foo.bar then don't add the package org too packages.txt, but rather org.foo or org.foo.bar.

# Whitelisting
If you want the tool to treat a method as pure you can add it to whitelist.txt. 
Doing this will ensure that no warnings are generated for methods that call the whitelisted method, even though the
method has side effects. 
Each methods in the file should be on its own line and with quotation marks.
For example, suppose that you want to whitelist a method named x declared in the class AST.A that accepts a string parameter and returns an AST.B object, then your whitelist.txt should have the following content:\
"AST.B AST.A#x (java.lang.String)"

# Analyzing Calls to External Methods
When the tool analyzes a call to a method defined in an external library the default behaviour is to only output if this
method call causes side effects, rather than analyzing the method further. For example, suppose you have a method that
calls java.util.List#add (java.lang.Object) and that this causes a side effect, the tool will in this case only generate
a warning that this method is called and that it causes a side effect. The alternative would be to analyze the
java.util.List#add (java.lang.Object) further, i.e., find any program statement that the method can execute that causes side effects.
This can, sometimes, be desirable if it is unclear why the call to an
external method causes side effects. Therefore, the tool supports adding external methods to the verbose-external.txt file to make the tool
do this kind of analysis for the added methods. 
The format of the methods added to the file is equivalent to the format for whitelist.txt, please refer to the "WhiteListing" section for more details.

# Analysis Summary
The tool will summarize the analysis after it has finished. The summary is found the in out/YYYY-MM-DD:HH:MM:SS/analysis-summary.txt where YYYY-MM-DD:HH:MM:SS is the date time of the analysis.
The top of the file contains information about the used configuration for the analysis. 
Below the configuration summary is a summary of impure methods.
An impure methods will either be in the "Aspect Methods" section or in the "Internal JastAdd Methods" section.
Impure methods that have been defined in an aspect file are found under the "Aspect Methods" label. 
Typically, these are the methods that are interesting to get purity information about. 
The reason for this is that there are many internal JastAdd methods 
that are expected to have side effects.
These methods, i.e., internal JastAdd methods are found below the label "Internal JastAdd Methods". 
The only exception to this are methods corresponding to rewrite equations. These methods are not explicitly given a name in the aspect file, and could in some sense be considered internal methods. 
They are, regardless, found under the "Aspect Methods" label.

# Dependencies
The tool has been verified to work with the following versions:\
Java JDK: 1.8.0_212\
Ant: 1.9.6\
Ubuntu: 16.04\
GNU bash: 4.3.48(1)-release (x86_64-pc-linux-gnu)

# Questions
Please contact joachim.wedin@gmail.com for any JPA related questions.
