package jp.ac.osakau.farseerfc.purano.jpa;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import jp.ac.osakau.farseerfc.purano.effect.Effect.SideEffectType;
import jp.ac.osakau.farseerfc.purano.util.JPAConfig.SideEffectMode;

public class MethodNode {

  private String label;
  private String className;
  private Set<String> edges;
  private List<String> fieldEffects;
  private List<String> argumentEffects;
  private List<String> staticEffects;
  private List<String> nativeEffects;

  public MethodNode(String label, String className) {
    this.label = label;
    this.className = className;
    edges = new HashSet<>();
    fieldEffects = new ArrayList<>();
    argumentEffects = new ArrayList<>();
    staticEffects = new ArrayList<>();
    nativeEffects = new ArrayList<>();
  }

  public String getClassName() {
    return className;
  }

  public String getLabel() {
    return label;
  }

  public void addEdge(String other) {
    edges.add(other);
  }

  public void addSideEffect(String se, SideEffectType type) {
    switch (type) {
    case FIELD:
      fieldEffects.add(se);
      return;
    case ARGUMENT:
      argumentEffects.add(se);
      return;
    case STATIC:
      staticEffects.add(se);
      return;
    case NATIVE:
      nativeEffects.add(se);
      return;
    default:
      return;
    }
  }

  public void dumpSummary(Stack<MethodNode> methodStack, StringBuilder sb) {

    List<String> effectsToDump = new ArrayList<>();
    effectsToDump.addAll(fieldEffects);
    effectsToDump.addAll(argumentEffects);
    effectsToDump.addAll(staticEffects);
    effectsToDump.addAll(nativeEffects);

    if (effectsToDump.size() > 0) {
      for (MethodNode m : methodStack) {
        sb.append(m.label);
        sb.append(" --->\n");
      }
      sb.append(label);
      sb.append(":\n");

      for (String s : effectsToDump) {
        sb.append("\t");
        sb.append(s);
        sb.append("\n");
      }

      sb.append("\n");
    }
  }

  private void dumpEffects(StringBuilder sb, List<List<String>> effectsToDump) {
    for (List<String> effectList : effectsToDump) {
      for (String s : effectList) {
        sb.append("<tr><td align=\"left\"><font color=\"red\">");
        sb.append(String.format("%s\n", s));
        sb.append("</font></td></tr>\n");
      }
    }
  }

  public void dump(StringBuilder sb, SideEffectMode mode) {
    sb.append("\n");
    sb.append("\"");
    sb.append(label);
    sb.append("\" [\n");
    sb.append(
        "label=<<table border=\"1\" cellborder=\"0\" cellspacing=\"1\">\n");
    sb.append("<tr><td align=\"center\"><font point-size=\"12\"><b>");
    String tmp = label;
    tmp = tmp.replaceAll("[<]", "&lt;");
    tmp = tmp.replaceAll("[>]", "&gt;");
    sb.append(tmp);
    sb.append("</b></font></td></tr>\n");

    List<List<String>> effectsToDump = new ArrayList<>();
    effectsToDump.add(fieldEffects);
    effectsToDump.add(argumentEffects);
    effectsToDump.add(staticEffects);
    effectsToDump.add(nativeEffects);
    dumpEffects(sb, effectsToDump);

    sb.append("</table>>");
    sb.append("];\n");
  }

  public void dumpTransitiveSummary(StringBuilder sb,
      Map<String, MethodNode> dotNodes, Map<String, Boolean> visited,
      Stack<MethodNode> methodStack, SideEffectMode mode) {
    visited.put(label, new Boolean(true));

    dumpSummary(methodStack, sb);

    methodStack.push(this);

    for (String otherName : edges) {
      if (visited.get(otherName) == null) {
        MethodNode other = dotNodes.get(otherName);
        other.dumpTransitiveSummary(sb, dotNodes, visited, methodStack, mode);
      }
    }

    methodStack.pop();
  }

  public void dumpTransitive(StringBuilder sb, Map<String, MethodNode> dotNodes,
      Map<String, Boolean> visited, SideEffectMode mode) {
    visited.put(label, new Boolean(true));

    dump(sb, mode);
    for (String otherName : edges) {
      MethodNode other = dotNodes.get(otherName);
      sb.append("\"");
      sb.append(label);
      sb.append("\"");
      sb.append(" -> ");
      sb.append("\"");
      sb.append(other.label);
      sb.append("\"");
      sb.append("\n");
    }

    for (String otherName : edges) {
      if (visited.get(otherName) == null) {
        MethodNode other = dotNodes.get(otherName);
        other.dumpTransitive(sb, dotNodes, visited, mode);
      }
    }
  }
}