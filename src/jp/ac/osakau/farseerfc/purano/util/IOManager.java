package jp.ac.osakau.farseerfc.purano.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class IOManager {

	public static void createDirectory(String directoryName) {
		File directory = new File(directoryName);
		boolean created = false;
		if (!directory.exists()) {
			created = directory.mkdir();
		}
	}

    public static void createFile(String path) {
    	try {
	      File file = new File(path);
	      file.createNewFile();
    	} catch (IOException e) {
	      e.printStackTrace();
		}
    }
    
	public static void resetFile(String path) {
		writeToFile(path, "");
	}

	public static void writeToFile(String path, List<String> data) {
		for (String line : data) {
			writeToFile(path, line);
		}
	}

	public static void writeToFile(String path, String data) {
		writeToFile(path, data, true);
	}

	public static void writeToFile(String path, String data, boolean append) {
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {

			fw = new FileWriter(path, append);
			bw = new BufferedWriter(fw);
			bw.write(data);

		} catch (IOException e) {

			e.printStackTrace();

		} finally {
			try {
				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}