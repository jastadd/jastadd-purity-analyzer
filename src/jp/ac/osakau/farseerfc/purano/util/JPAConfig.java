package jp.ac.osakau.farseerfc.purano.util;

import java.util.ArrayList;
import java.util.List;

import jp.ac.osakau.farseerfc.purano.reflect.MethodRep;

public class JPAConfig {
  public enum SideEffectMode {
    DEFAULT, NO_STATIC, STATIC, NATIVE
  }

  public final boolean findCacheSemantics;
  public final boolean propagateToExternal;
  public final SideEffectMode mode;
  public final int effectConfig;
  private final boolean extendedAnalysisForAll;
  private List<String> whiteList;
  private List<String> verboseExternal;
  private List<String> extendedAnalysis;

  public JPAConfig() {
    this.mode = SideEffectMode.DEFAULT;
    this.findCacheSemantics = true;
    this.propagateToExternal = true;
    effectConfig = 7;
    extendedAnalysisForAll = false;
    whiteList = new ArrayList<>();
    verboseExternal = new ArrayList<>();
    extendedAnalysis = new ArrayList<>();
  }

  public JPAConfig(SideEffectMode mode, List<String> whiteList,
      List<String> verboseExternal, List<String> extendedAnalysis,
      boolean extendedAnalysisForAll, boolean findCacheSemantics,
      boolean propagateToExternal) {
    this.mode = mode;
    this.findCacheSemantics = findCacheSemantics;
    this.extendedAnalysisForAll = extendedAnalysisForAll;
    this.propagateToExternal = propagateToExternal;
    this.whiteList = whiteList;
    this.verboseExternal = verboseExternal;
    this.extendedAnalysis = extendedAnalysis;
    switch (mode) {
    case DEFAULT:
    case STATIC:
      effectConfig = 7;
      break;
    case NO_STATIC:
      effectConfig = 3;
      break;
    case NATIVE:
      effectConfig = 8;
      break;
    default:
      effectConfig = 7;
      break;
    }
  }

  public boolean createFieldEffects() {
    return (effectConfig & 0x1) > 0;
  }

  public boolean createArgumentEffects() {
    return (effectConfig & 0x2) > 0;
  }

  public boolean createStaticEffects() {
    return (effectConfig & 0x4) > 0;
  }

  public boolean createNativeEffects() {
    return (effectConfig & 0x8) > 0;
  }

  public boolean whiteListed(MethodRep rep) {
    return whiteList.contains(rep.toString());
  }

  public boolean verboseExternal(MethodRep rep) {
    return verboseExternal.contains(rep.toString());
  }

  public boolean extendedAnalysis(String className) {
    return (extendedAnalysisForAll || extendedAnalysis.contains(className));
  }
}
