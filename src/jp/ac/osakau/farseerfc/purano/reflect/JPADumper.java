package jp.ac.osakau.farseerfc.purano.reflect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import javax.annotation.Nullable;

import org.jetbrains.annotations.NotNull;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.util.Printer;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import jp.ac.osakau.farseerfc.purano.dep.DepEffect;
import jp.ac.osakau.farseerfc.purano.dep.DepFrame;
import jp.ac.osakau.farseerfc.purano.effect.Effect;
import jp.ac.osakau.farseerfc.purano.effect.Effect.SideEffectType;
import jp.ac.osakau.farseerfc.purano.effect.FieldEffect;
import jp.ac.osakau.farseerfc.purano.jpa.MethodNode;
import jp.ac.osakau.farseerfc.purano.util.Escaper;
import jp.ac.osakau.farseerfc.purano.util.IOManager;
import jp.ac.osakau.farseerfc.purano.util.JPAConfig;
import jp.ac.osakau.farseerfc.purano.util.JPAConfig.SideEffectMode;
import jp.ac.osakau.farseerfc.purano.util.Types;

public class JPADumper implements ClassFinderDumpper {
	private final ClassFinder cf;
	private final Escaper esc;
	private final Types table;
	Map<String, MethodNode> methodNodes;
	private final String outputDir;
	private final JPAConfig config;

	public JPADumper(ClassFinder cf, String outputDir, JPAConfig config) throws IOException {
		this.cf = cf;
		this.esc = Escaper.getDot();
		this.table = new Types(false);
		methodNodes = new TreeMap<>();
		this.outputDir = outputDir;
		this.config = config;
	}

	public JPADumper(ClassFinder cf, String outputDir) throws IOException {
		this.cf = cf;
		this.esc = Escaper.getDot();
		this.table = new Types(false);
		methodNodes = new TreeMap<>();
		this.outputDir = outputDir;
		this.config = new JPAConfig();
	}

	public void createMethodNode(MethodRep m) {
		MethodNode methodNode = methodNodes.get(m.toString());
		if (methodNode == null) {
			String className = m.getClassRep().getName();
			MethodNode node = new MethodNode(m.toString(), className);
			methodNodes.put(m.toString(), node);
		}
	}

	public boolean isTargeted(String clsName) {
		for (String prefix : cf.prefix) {
			if (clsName.startsWith(prefix)) {
				return true;
			}
		}
		return cf.classTargets.contains(clsName);
	}

	public int getLine(DepFrame frame) {
		int line = -1;
		if (frame.getLine() != null) {
			line = frame.getLine().line;
		}
		return line;
	}

	public boolean validFrame(DepFrame frame) {
		if (frame == null) {
			return false;
		}
		AbstractInsnNode node = frame.getNode();
		return (node.getOpcode() > 0 && node.getOpcode() < Printer.OPCODES.length);
	}

	public void processOverrideEffects(MethodRep m, Map<Effect, Boolean> handledEffects) {
		DepEffect dep = m.getDynamicEffects();
		for (Effect e : dep.getAllEffects()) {
			if (handledEffects.get(e) == null) {
				processOverrideEffect(e, m);
				handledEffects.put(e, true);
			}
		}
	}

	public void processOverrideEffect(Effect e, MethodRep rep) {
		if (e.isDirect()) {
			return;
		}
		String recieverName = rep.toString();
		String originName = e.getFrom().toString();

		MethodNode reciever = createMethodNode(recieverName, rep);
		MethodNode origin = createMethodNode(originName, e.getFrom());

		reciever.addEdge(originName);

		methodNodes.put(recieverName, reciever);
		methodNodes.put(originName, origin);
		return;
	}

	public void processFrameEffects(DepFrame frame, MethodRep m, DepEffect dep, Map<Effect, Boolean> handledEffects) {
		String prefix = "";
		String line = "line=" + getLine(frame);
		for (Effect e : dep.getAllEffects()) {
			processEffect(e, m, table, prefix, esc, line, methodNodes, cf);
			handledEffects.put(e, true);
		}
	}

	public void processEffect(Effect e, MethodRep rep, @NotNull Types table, String prefix, Escaper esc, String line,
			Map<String, MethodNode> methodNodes, ClassFinder cf) {
		if (e.isDirect()) {
			processDirectEffect(e, rep, table, prefix, esc, line, methodNodes);
		} else {
			processTransitiveEffect(e, rep, table, prefix, esc, line, methodNodes, cf);
		}
	}

	public String getEffectInfo(Effect e, MethodRep rep, String line) {
		String tmp = getClass().getSimpleName();
		tmp = tmp.substring(0, tmp.length() - 6);
		ArrayList<String> result = new ArrayList<>(
				Lists.transform(e.dumpEffect(rep, table), new Function<String, String>() {
					@Nullable
					@Override
					public String apply(@Nullable String s) {
						return esc.effect(s);
					}
				}));
		result.add(line);
		if (e.getFrom() != null) {
			String fromStr = esc.from("from = \""
					+ table.dumpMethodDesc(e.getFrom().getInsnNode().desc, String.format("%s#%s",
							table.fullClassName(e.getFrom().getInsnNode().owner), e.getFrom().getInsnNode().name))
					+ "\"");
			result.add(fromStr);
		}
		return String.format("%s@%s(%s)", "", esc.annotation(e.getClass().getSimpleName()),
				Joiner.on(", ").join(result));
	}

	public void processDirectEffect(Effect e, MethodRep rep, @NotNull Types table, String prefix, Escaper esc,
			String line, Map<String, MethodNode> methodNodes) {

		String effectInfo = getEffectInfo(e, rep, line);
		MethodNode node = createMethodNode(rep.toString(), rep);
		if (rep.isInit() && e instanceof FieldEffect) {
			// do nothing
		} else {
			if (config.mode == SideEffectMode.DEFAULT) {
				node.addSideEffect(effectInfo, e.type());
			} else if (config.mode == SideEffectMode.NO_STATIC) {
				if (e.type() == SideEffectType.FIELD || e.type() == SideEffectType.ARGUMENT) {
					node.addSideEffect(effectInfo, e.type());
				}
			} else if (config.mode == SideEffectMode.STATIC && e.type() == SideEffectType.STATIC) {
				node.addSideEffect(effectInfo, e.type());
			} else if (config.mode == SideEffectMode.NATIVE && e.type() == SideEffectType.NATIVE) {
				node.addSideEffect(effectInfo, e.type());
			}
		}
		methodNodes.put(rep.toString(), node);
	}

	private boolean staticEffectOrigin(Effect e) {
		return e.type() == SideEffectType.STATIC && e.getFrom().getDynamicEffects().getStaticField().size() == 0;
	}

	public void processTransitiveEffect(Effect e, MethodRep rep, @NotNull Types table, String prefix, final Escaper esc,
			String line, Map<String, MethodNode> methodNodes, ClassFinder cf) {
		String recieverName = rep.toString();
		String originName = e.getFrom().toString();

		MethodNode reciever = createMethodNode(recieverName, rep);
		MethodNode origin = createMethodNode(originName, e.getFrom());

		if (staticEffectOrigin(e) && config.mode == SideEffectMode.STATIC) {
			String effectInfo = getEffectInfo(e, rep, line);
			reciever.addSideEffect(effectInfo, SideEffectType.STATIC);
      methodNodes.put(recieverName, reciever);
			return;
		}

		if (!isTargeted(e.getFrom().getClassRep().getName()) && !config.propagateToExternal && !cf.config.verboseExternal(e.getFrom())) {
      String effectInfo = getEffectInfo(e, rep, line);
      reciever.addSideEffect(effectInfo, e.type());
		} else if (e.getFrom().isAbstract() || e.getFrom().getOverrided().size() > 0) {
			reciever.addEdge(originName);
			for (MethodRep overrider : e.getFrom().getOverrided().values()) {
				DepEffect oe = overrider.getDynamicEffects();
				if (oe.getArgumentEffects().size() > 0 || oe.getCallEffects().size() > 0
						|| oe.getOtherField().size() > 0 || oe.getStaticField().size() > 0
						|| oe.getThisField().size() > 0) {
					String overriderName = overrider.toString();
					MethodNode o = createMethodNode(overriderName, overrider);
					methodNodes.put(overriderName, o);
					origin.addEdge(overriderName);
				}
			}
		} else {
			if (config.mode == SideEffectMode.DEFAULT) {
				reciever.addEdge(originName);
			} else if (config.mode == SideEffectMode.NO_STATIC) {
				DepEffect de = e.getFrom().getDynamicEffects();
				if (de.getThisField().size() > 0 || de.getArgumentEffects().size() > 0) {
					reciever.addEdge(originName);
				}
			} else if (config.mode == SideEffectMode.STATIC) {
				if (e.getFrom().getDynamicEffects().getStaticField().size() > 0) {
					reciever.addEdge(originName);
				}
			} else if (config.mode == SideEffectMode.NATIVE) {
				if (e.getFrom().getDynamicEffects().getOtherEffects().size() > 0) {
          String effectInfo = getEffectInfo(e, rep, line);
          reciever.addSideEffect(effectInfo, e.type());
				}
			}
		}

		methodNodes.put(recieverName, reciever);
		methodNodes.put(originName, origin);
		return;
	}

	public MethodNode createMethodNode(String name, MethodRep rep) {
		MethodNode methodNode = methodNodes.get(name);
		if (methodNode == null) {
			methodNode = new MethodNode(name, rep.getClassRep().getName());
			methodNodes.put(name, methodNode);
		}
		return methodNode;
	}

	public List<String> initResult(Effect e, final Escaper esc, MethodRep rep, @NotNull Types table) {
		String className = getClass().getSimpleName();
		className = className.substring(0, className.length() - 6);
		ArrayList<String> result = new ArrayList<>(
				Lists.transform(e.dumpEffect(rep, table), new Function<String, String>() {
					@Nullable
					@Override
					public String apply(@Nullable String s) {
						return esc.effect(s);
					}
				}));

		return result;
	}

	public void processMethod(MethodRep m) {
		// create node if it does not exist
		createMethodNode(m);

		Map<Effect, Boolean> handledEffects = new HashMap<Effect, Boolean>();
		if (m.getDynamicEffects() != null) {
			if (m.getFrames() != null) {
				for (int i = 0; i < m.getFrames().length; ++i) {
					DepFrame frame = m.getFrames()[i];
					if (validFrame(frame)) {
						processFrameEffects(frame, m, m.getFrameEffects()[i], handledEffects);
					}
				}
			}
		}
		processOverrideEffects(m, handledEffects);
	}

	public void processClass(ClassRep cls) {
		for (MethodRep m : cls.getAllMethods()) {
			if (m.getMethodNode() != null) {
				processMethod(m);
			}
		}
	}

	public void createMethodNodes() {
		for (String clsName : cf.classMap.keySet()) {
//			if (isTargeted(clsName)) {
			processClass(cf.classMap.get(clsName));
//			}
		}
	}

	public void dumpHeader(StringBuilder sb) {
		sb.append("digraph analysis {\n");
		sb.append("node [shape=plaintext fontname=\"Sans serif\" fontsize=\"8\"];\n");
	}

	public void dumpEnd(StringBuilder sb) {
		sb.append("\n}");
	}

	public String dumpMethodNodeSummary(MethodNode node) {
		StringBuilder sb = new StringBuilder();
		node.dumpTransitiveSummary(sb, methodNodes, new HashMap<String, Boolean>(), new Stack<MethodNode>(), config.mode);
		return sb.toString();
	}

	public String dumpMethodNode(MethodNode node) {
		StringBuilder sb = new StringBuilder();
		dumpHeader(sb);
		node.dumpTransitive(sb, methodNodes, new HashMap<String, Boolean>(), config.mode);
		dumpEnd(sb);
		return sb.toString();
	}

	public String classDir(MethodNode node) {
		String dir = outputDir + node.getClassName();
		return dir.replaceAll(" ", "_");
	}

	public String summaryDir(MethodNode node) {
		String dir = classDir(node) + "/sideeffect_summary/";
		return dir.replaceAll(" ", "_");
	}

	public String summaryFileName(MethodNode node) {
		String fileName = node.getLabel() + ".txt";
		return fileName.replaceAll(" ", "_");
	}

	public String dotDir(MethodNode node) {
		String dir = outputDir + node.getClassName() + "/dot/";
		return dir.replaceAll(" ", "_");
	}

	public String dotFileName(MethodNode node) {
		String fileName = node.getLabel() + ".dot";
		return fileName.replaceAll(" ", "_");
	}

	public void createOutputDir() {
		IOManager.createDirectory(outputDir);
	}

	public void createClassDir(MethodNode node) {
		IOManager.createDirectory(classDir(node));
	}

	public void createDotDir(MethodNode node) {
		IOManager.createDirectory(dotDir(node));
	}

	public void outputMethodNode(MethodNode node) {
		IOManager.writeToFile(dotDir(node) + dotFileName(node), dumpMethodNode(node));
	}

	public void outputMethodNodeSummary(MethodNode node) {
		IOManager.writeToFile(summaryDir(node) + summaryFileName(node), dumpMethodNodeSummary(node));
	}

	public void createSummaryDir(MethodNode node) {
		IOManager.createDirectory(summaryDir(node));
	}
	
	public void dumpMethodNodes() {
		createOutputDir();

		for (String key : methodNodes.keySet()) {
			MethodNode node = methodNodes.get(key);
			createClassDir(node);
			createDotDir(node);
			if (config.extendedAnalysis(node.getClassName())) {
				outputMethodNode(node);
			}
		}
	}

	public boolean dumpMethodNode(String nodeName, String outputFile) {
		MethodNode node = methodNodes.get(nodeName);
		if (node == null) {
			return false;
		}
		IOManager.writeToFile(outputFile, dumpMethodNode(node), false);
		return true;
	}

	public void dumpSideEffectSummary() {
		for (String key : methodNodes.keySet()) {
			MethodNode node = methodNodes.get(key);
			createSummaryDir(node);
			if (config.extendedAnalysis(node.getClassName())) {
				outputMethodNodeSummary(node);
			}
		}
	}

	@Override
	public void dump() {
		createMethodNodes();
		dumpMethodNodes();
		dumpSideEffectSummary();
	}
}