package jp.ac.osakau.farseerfc.purano.reflect;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import com.google.common.base.Joiner;

import AST.JPAAnnotation;
import jp.ac.osakau.farseerfc.purano.dep.DepEffect;
import jp.ac.osakau.farseerfc.purano.util.IOManager;
import jp.ac.osakau.farseerfc.purano.util.JPAConfig;
import jp.ac.osakau.farseerfc.purano.util.JPAConfig.SideEffectMode;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ClassFinder {

  public Path findSourcePath(String name) {
    for (String srcPrefix : sourcePrefix) {
      String className = name.replace(".", "/") + ".java";
      Path path = Paths.get(srcPrefix, className);
      File file = path.toFile();
      if (file.exists() && file.isFile()) {
        return path;
      }
    }
    // log.info("Not found source for {}", name);
    return null;
  }

  @Getter
  final Map<String, ClassRep> classMap = new HashMap<>();

  public final Set<String> classTargets = new HashSet<>();
  public final List<String> prefix;

  private static final int MAX_LOAD_PASS = 2;

  private final boolean examChangedSignatures = true;
  private final boolean breakForloop = true;
  private @NotNull final List<String> sourcePrefix;
  public final JPAConfig config;

  public ClassFinder(@NotNull List<String> prefix,
      @NotNull List<String> sourcePrefix) {
    this.sourcePrefix = sourcePrefix;
    findTargetClasses(prefix);
    this.prefix = prefix;
    this.config = new JPAConfig();
  }

  public ClassFinder(@NotNull List<String> prefix,
      @NotNull List<String> sourcePrefix, JPAConfig config) {
    this.sourcePrefix = sourcePrefix;
    findTargetClasses(prefix);
    this.prefix = prefix;
    this.config = config;
  }

  public ClassFinder(@NotNull List<String> prefix) {
    this(prefix, new ArrayList<String>());
  }

  public ClassFinder(@NotNull List<String> prefix, JPAConfig config) {
    this(prefix, new ArrayList<String>(), config);
  }

  public ClassFinder(String string) {
    this(Arrays.asList(string));
  }

  public void resolveMethods() {
    int timestamp = 0;
    Set<ClassRep> allCreps = new HashSet<>(classMap.values());
    boolean changed;
    int pass = 0;
    int changedMethod = 0;

    List<Integer> changedMethodsTrace = new ArrayList<>();
    List<Integer> loadedClassesTrace = new ArrayList<>();
    do {
      changed = false;
      if (pass < MAX_LOAD_PASS) {
        allCreps = new HashSet<>(classMap.values());
        loadedClassesTrace.add(allCreps.size());
      }
      changedMethod = 0;

      Set<MethodRep> changedSignatures = new HashSet<>();
      for (ClassRep crep : allCreps) {
        for (MethodRep mrep : crep.getAllMethods()) {
          if (mrep.isNeedResolve(this)) {
            if (mrep.resolve(++timestamp, this)) {
              changed = true;
              changedMethod++;
              if (examChangedSignatures) {
                changedSignatures.add(mrep);
              }
            }
          }
        }
      }
      changedMethodsTrace.add(changedMethod);
      // log.info(String.format("Pass: %d Classes: %s Changed Method: %d [%s]",
      // pass++, allCreps.size(), changedMethod,
      // Joiner.on(", ").join(changedMethodsTrace)));
      System.out.println(
          String.format("Pass: %d Changed Method: %d", pass++, changedMethod));
      if (examChangedSignatures) {
        final int maxdump = 2;
        if (changedMethod > maxdump) {
          MethodRep[] top = new MethodRep[maxdump];
          int i = 0;
          for (MethodRep mid : changedSignatures) {
            if (i >= maxdump)
              break;
            top[i++] = mid;
          }
          // log.info(Joiner.on(", ").join(top));
        } else {
          // for(MethodRep m:changedSignatures){
          // log.info(Joiner.on("\n").join(m.dump(this, new Types(),
          // Escaper.getDummy())));
          // }
          if (breakForloop) {
            break;
          }
        }
      }
    } while (changed);

    // log.info("Loaded Classes: " + Joiner.on(", ").join(loadedClassesTrace));
    // log.info("Changed methods: " + Joiner.on(",
    // ").join(changedMethodsTrace));
  }

  private void findTargetClasses(@NotNull Collection<String> prefixes) {

    for (String prefix : prefixes) {
      Reflections reflections = new Reflections(prefix,
          new SubTypesScanner(false));
      classTargets
          .addAll(reflections.getStore().getSubTypesOf(Object.class.getName()));
      classTargets.add(prefix);
    }
    for (String cls : classTargets) {
      loadClass(cls);
    }
  }

  public ClassRep loadClass(@NotNull String classname) {
    if (!classMap.containsKey(classname)) {
      // log.info("Loading {}", classname);
      if (classname.startsWith("[")) {
        classMap.put(classname, new ClassRep(ArrayStub.class.getName(), this));
      } else {
        classMap.put(classname, new ClassRep(classname, this));
      }
    }
    return classMap.get(classname);
  }

  public static boolean isTargeted(ClassFinder cf, String clsName) {
    for (String prefix : cf.prefix) {
      if (clsName.startsWith(prefix)) {
        return true;
      }
    }
    return cf.classTargets.contains(clsName);
  }

  public static void summarizeAnalysis(ClassFinder cf, JPAConfig config,
      String dateString, String analysisTime, String configPrettyPrint,
      String dest) {
    IOManager.writeToFile(dest, "Analysis date: " + dateString + "\n\n");
    IOManager.writeToFile(dest, "Total analysis time: " + analysisTime + " seconds" + "\n\n");
    IOManager.writeToFile(dest, configPrettyPrint);

    IOManager.writeToFile(dest, "\n\n*************************\n");
    IOManager.writeToFile(dest, "* Impure Aspect Methods *\n");
    IOManager.writeToFile(dest, "*************************\n");

    List<String> nonDeveloperMethods = new ArrayList<String>();
    for (String clsName : cf.classMap.keySet()) {
      if (isTargeted(cf, clsName)) {
        ClassRep cls = cf.classMap.get(clsName);
        try {
          Class c = Class.forName(clsName);
          Method[] methods = c.getDeclaredMethods();
          for (Method reflectMethod : methods) {
            String reflectMethodName = reflectMethod.toString()
                .replaceAll("[.]([^.]+)[(]", "#$1 (")
                .replaceFirst("private ", "").replaceFirst("protected ", "")
                .replaceFirst("public ", "");

            // find the corresponding method representation
            for (MethodRep methodRep : cls.getAllMethods()) {
              if (methodRep.toString().equals(reflectMethodName)) {
                // found it
                DepEffect de = methodRep.getDynamicEffects();

                boolean nonDeveloperMethod = false;
                for (Annotation a : reflectMethod.getAnnotations()) {
                  String[] parts = a.annotationType().toString().split("[$]");
                  if (parts[parts.length - 1].equals("NonDeveloper")) {
                    nonDeveloperMethod = true;
                    break;
                  }
                }

                if (de.getArgumentEffects().size() > 0
                    || de.getCallEffects().size() > 0
                    || de.getOtherField().size() > 0
                    || de.getStaticField().size() > 0
                    || de.getThisField().size() > 0) {
                  if (nonDeveloperMethod && !methodRep.isInit()) {
                    nonDeveloperMethods.add(methodRep.toString());
                  } else {
                    IOManager.writeToFile(dest, methodRep.toString() + "\n");
                  }
                }
              }
            }
          }
        } catch (Exception e) {
        }
      }
    }

    IOManager.writeToFile(dest, "\n***********************************\n");
    IOManager.writeToFile(dest, "* Impure Internal JastAdd Methods *\n");
    IOManager.writeToFile(dest, "***********************************\n");
    for (String m : nonDeveloperMethods) {
      IOManager.writeToFile(dest, m.toString() + '\n');
    }
  }

  public static void analyse(SideEffectMode mode, List<String> targetPackages,
      List<String> whiteList, List<String> verboseExternal,
      List<String> extendedAnalysis, boolean extendedAnalysisForAll,
      String configPrettyPrint) throws IOException {

    System.out.println("The following configuration is used:\n");
    System.out.println(configPrettyPrint);

    long start = System.currentTimeMillis();
    JPAConfig config = new JPAConfig(mode, whiteList, verboseExternal,
        extendedAnalysis, extendedAnalysisForAll, true, false);

    System.out.println("\nBuilding side effect graph...");
    ClassFinder cf = new ClassFinder(targetPackages, config);
    cf.resolveMethods();
    System.out.println("Done.");

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
    String dateString = dateFormat.format(new Date());

    String outputDir = "./out/" + dateString + "/";

    System.out.println("\nGenerating HTML output...");
    HtmlDumpper dumpper = new HtmlDumpper(System.out, cf, outputDir);
    dumpper.dump();
    System.out.println("Done.");

    System.out.println("\nGenerating extended analysis output...");
    JPADumper dd = new JPADumper(cf, outputDir, config);
    dd.dump();
    System.out.println("Done.");

    String analysisTime = "" + (System.currentTimeMillis() - start) / 1000;
    System.out.println("\nAnalysis time: " + analysisTime + " seconds");

    System.out.println("\nSummarizing analysis...");
    summarizeAnalysis(cf, config, dateString, analysisTime, configPrettyPrint,
        outputDir + "/analysis-summary.txt");
    System.out.println("Done.");
  }

  public static void main(@NotNull String[] args)
      throws IOException, ParseException {
    Options options = new Options();
    options.addOption(Option.builder().longOpt("analysismode").required(false)
        .hasArg().valueSeparator().build());
    options.addOption(Option.builder().longOpt("packages").required(false)
        .hasArg().valueSeparator().build());
    options.addOption(Option.builder().longOpt("sourcedirs").required(false)
        .hasArg().valueSeparator().build());
    options.addOption(Option.builder().longOpt("whitelist").required(false)
        .hasArg().valueSeparator().build());
    options.addOption(Option.builder().longOpt("verboseexternal")
        .required(false).hasArg().valueSeparator().build());
    options.addOption(Option.builder().longOpt("extendedanalysis")
        .required(false).hasArg().valueSeparator().build());
    options.addOption(Option.builder().longOpt("extendedanalysisforall")
        .required(false).hasArg().valueSeparator().build());

    CommandLineParser parser = new DefaultParser();
    CommandLine cmd = parser.parse(options, args);

    List<String> targetPackages = new ArrayList<>();
    List<String> sourceDirs = new ArrayList<>();
    List<String> whiteList = new ArrayList<>();
    List<String> verboseExternal = new ArrayList<>();
    List<String> extendedAnalysis = new ArrayList<>();
    boolean extendedAnalysisForAll = false;

    SideEffectMode mode;
    if (cmd.hasOption("analysismode")) {
      String m = cmd.getOptionValues("analysismode")[0];
      if (m.equals("default")) {
        mode = SideEffectMode.DEFAULT;
      } else if (m.equals("no_static")) {
        mode = SideEffectMode.NO_STATIC;
      } else if (m.equals("static")) {
        mode = SideEffectMode.STATIC;
      } else if (m.equals("native")) {
        mode = SideEffectMode.NATIVE;
      } else {
        System.out.println("Unrecognized analysis mode: " + m);
        System.out.println("The default analysis mode will be used.");
        mode = SideEffectMode.DEFAULT;
      }
    } else {
      mode = SideEffectMode.DEFAULT;
    }

    targetPackages.add("AST");
    if (cmd.hasOption("packages")) {
      String packages = cmd.getOptionValues("packages")[0];
      targetPackages.addAll(Arrays.asList(packages.split(":")));
    }

    if (cmd.hasOption("sourcedirs")) {
      String packages = cmd.getOptionValues("sourcedirs")[0];
      sourceDirs.addAll(Arrays.asList(packages.split(":")));
    }

    if (cmd.hasOption("whitelist")) {
      String wl = cmd.getOptionValues("whitelist")[0];
      whiteList.addAll(Arrays.asList(wl.split(":")));
    }

    if (cmd.hasOption("verboseexternal")) {
      String ve = cmd.getOptionValues("verboseexternal")[0];
      verboseExternal.addAll(Arrays.asList(ve.split(":")));
    }

    if (cmd.hasOption("extendedanalysis")) {
      String ea = cmd.getOptionValues("extendedanalysis")[0];
      extendedAnalysis.addAll(Arrays.asList(ea.split(":")));
    }

    if (cmd.hasOption("extendedanalysisforall")) {
      String eafa = cmd.getOptionValues("extendedanalysisforall")[0];
      extendedAnalysisForAll = Boolean.parseBoolean(eafa);
    }

    String configPrettyPrint = prettyPrintConfig(mode, targetPackages,
        sourceDirs, whiteList, verboseExternal, extendedAnalysis,
        extendedAnalysisForAll);

    analyse(mode, targetPackages, whiteList, verboseExternal, extendedAnalysis,
        extendedAnalysisForAll, configPrettyPrint);
  }

  private static String prettyPrintConfig(SideEffectMode mode,
      List<String> targetPackages, List<String> sourceDirs,
      List<String> whiteList, List<String> verboseExternal,
      List<String> extendedAnalysis, boolean extendedAnalysisForAll) {

    StringBuilder sb = new StringBuilder();

    sb.append("Analysis mode:\n");
    sb.append(mode.toString());
    sb.append("\n");

    sb.append("\nAnalyzed packages:\n");
    targetPackages.forEach(x -> sb.append(x.toString() + "\n"));

    sb.append("\nAnalyzed source directories:\n");
    sourceDirs.forEach(x -> sb.append(x.toString() + "\n"));

    sb.append("\nWhitelisted methods:\n");
    whiteList.forEach(x -> sb.append(x.toString() + "\n"));

    sb.append("\nVerbose external:\n");
    verboseExternal.forEach(x -> sb.append(x.toString() + "\n"));

    sb.append("\nExtended analysis:\n");
    extendedAnalysis.forEach(x -> sb.append(x.toString() + "\n"));

    sb.append("\nExtended analysis for all:\n");
    sb.append(extendedAnalysisForAll);

    return sb.toString();
  }
}
