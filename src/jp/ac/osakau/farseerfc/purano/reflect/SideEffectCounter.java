package jp.ac.osakau.farseerfc.purano.reflect;

import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.util.Printer;

import jp.ac.osakau.farseerfc.purano.dep.DepEffect;
import jp.ac.osakau.farseerfc.purano.dep.DepFrame;
import jp.ac.osakau.farseerfc.purano.effect.Effect;

public class SideEffectCounter {

	private ClassFinder cf;

	public SideEffectCounter(ClassFinder cf) {
		this.cf = cf;
	}

	public boolean isTargeted(String clsName) {
		for (String prefix : cf.prefix) {
			if (clsName.startsWith(prefix)) {
				return true;
			}
		}
		return cf.classTargets.contains(clsName);
	}

	private int countConstructorSideEffects(MethodRep m) {
		DepEffect de = m.getDynamicEffects();
		int count = de.getArgumentEffects().size();
		count += de.getStaticField().size();
		return count;
	}

	public boolean validFrame(DepFrame frame) {
		if (frame == null) {
			return false;
		}
		AbstractInsnNode node = frame.getNode();
		return (node.getOpcode() > 0 && node.getOpcode() < Printer.OPCODES.length);
	}

//	public int processFrameEffects(DepFrame frame, MethodRep m, DepEffect dep) {
//	  int count = 0;
//		for (Effect e : dep.getAllEffects()) {
//		  count++;
//		}
//		
//		return count;
//	}

	private int countMethodSideEffects(MethodRep m) {
//	  int count = 0;
//    if (m.getDynamicEffects() != null) {
//			if (m.getFrames() != null) {
//				for (int i = 0; i < m.getFrames().length; ++i) {
//					DepFrame frame = m.getFrames()[i];
//					if (validFrame(frame)) {
//						count += processFrameEffects(frame, m, m.getFrameEffects()[i]);
//					}
//				}
//			}
//		}
		DepEffect de = m.getDynamicEffects();
		int count = de.getArgumentEffects().size();
//		count += de.getOtherField().size();
		count += de.getThisField().size();
//		count += de.getStaticField().size();
		return count;
	}

	private int countClassSideEffects(ClassRep cls) {
		int count = 0;
		for (MethodRep m : cls.getAllMethods()) {
			if (m.isInit()) {
				count += countConstructorSideEffects(m);
			} else {
				count += countMethodSideEffects(m);
			}
		}
		return count;
	}

	public int countAllSideEffects() {
		int count = 0;
		for (String clsName : cf.classMap.keySet()) {
			if (isTargeted(clsName)) {
				count += countClassSideEffects(cf.classMap.get(clsName));
			}
		}
		return count;
	}
}
