package no_static_mode;

public class A {
	public int x;
	public static int y;
	public void f() {
		g();
		y++;
		x++;
	}
	public void g() {
		h();
	}
	public void h() {
		y++;
		k();
	}

	public void k() {
		x++;
		l();
	}

	public void l() {
		x++;
		h();
		i(this);
	}
	public void i(A a) {
		a.x++;
		a.h();
	}
}
