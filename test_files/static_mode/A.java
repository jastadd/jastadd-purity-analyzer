package static_mode;

public class A {
	public static A a = new A();
	public int x;
	public static int y;
	public void f() {
		g();
		h();
	}
	public void g() {
		x++;
	}
	public void h() {
		i(a);
		j(a);
	}

	public void i(A a) { 
		a.x++;
	}

	public void j(A a) {
		l();
		a.y++;
	}
	

	public void l() { 
		y++;
	}
}
