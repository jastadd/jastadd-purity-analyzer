package transitive_field_effect;

public class A {
	int x = 0;
	public void f() {
		g();
	}

	public void g() {
		x++;
	}
}
