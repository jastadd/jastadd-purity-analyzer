package override_test;

public class A {
	B b = new C();

	public void f() {
		b.g();
	}
	
	public class C extends B {
		@Override
		public void g() {
			y++;
		}
	}

	public class B {
		public int y = 0;
		public void g() {
			y--;
		}
	}
}