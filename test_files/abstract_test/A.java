package abstract_test;

public class A {
	B b = new C();
	public void f() {
		b.g();
	}
	
	public abstract class B {
		public int x = 0;
		public abstract void g();
	}

	public class C extends B {
		@Override
		public void g() {
			x++;
		}
	}
}
