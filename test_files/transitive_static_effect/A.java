package transitive_static_effect;

public class A {
	public int x;
	static A a;
	public void f() {
		g();
	}

	public void g() {
		h(a);
	}

	public void h(A arg) {
		arg.x++;
	}
}
