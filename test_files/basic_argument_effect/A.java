package basic_argument_effect;

public class A {
	A a = new A();
	int x;
	public void f() {
		g(a);
	}
	public void g(A arg) {
		arg.x++;
	}
}
