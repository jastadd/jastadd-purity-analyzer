/* Copyright (c) 2013, Jesper Öqvist <jesper@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.tinytemplate.fragment;

import java.io.PrintStream;

import org.jastadd.tinytemplate.Indentation;
import org.jastadd.tinytemplate.TemplateContext;

/**
 * Reflectively expands an attribute in the template context
 * @author Jesper Öqvist <jesper@llbit.se>
 */
public class AttributeReference extends NestedIndentationFragment {

	private final String attribute;

	/**
	 * @param attributeName
	 */
	public AttributeReference(String attributeName) {
		attribute = attributeName;
	}

	@Override
	public void expand(TemplateContext context, StringBuilder out) {
		expandWithIndentation(String.valueOf(context.evalAttribute(attribute)),
				context, out);
	}

	@Override
	public String toString() {
		return "#(" + attribute + ")";
	}

	@Override
	public boolean isKeyword(String varName) {
		return attribute.equals(varName);
	}

	@Override
	public void printAspectCode(Indentation ind, int lvl, PrintStream out) {
		out.println(ind.get(lvl) + "out.print(" + attribute + "());");
	}
}
