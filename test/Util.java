import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Scanner;

/** Utility methods for running tests. */
public final class Util {
	private static String SYS_LINE_SEP = System.getProperty("line.separator");

	private Util() { }

	/**
	 * Check that the string matches the contents of the given file.
	 * Also writes the actual output to file.
	 * @param actual actual output
	 * @param out file where output should be written
	 * @param expected file containing expected output
	 */
	public static void compareOutput(String actual, File out, File expected) {
		try {
			Files.write(out.toPath(), actual.getBytes());
			assertEquals("Output differs",
				readFileToString(expected),
				normalizeText(actual));
		} catch (IOException e) {
			fail("IOException occurred while comparing output: " + e.getMessage());
		}
	}

	public static void compareOutput(File actual, File expected) {
		try {
			assertEquals("Output differs", readFileToString(expected), readFileToString(actual));
		} catch (IOException e) {
			fail("IOException occurred while comparing output: " + e.getMessage());
		}
	}

	/**
	 * Reads an entire file to a string object.
	 * <p>If the file does not exist an empty string is returned.
	 * <p>The system dependent line separator char sequence is replaced by
	 * the newline character.
	 *
	 * @return normalized text from file
	 */
	private static String readFileToString(File file) throws FileNotFoundException {
		if (!file.isFile()) {
			return "";
		}

		Scanner scanner = new Scanner(file);
		scanner.useDelimiter("\\Z");
		String text = normalizeText(scanner.hasNext() ? scanner.next() : "");
		scanner.close();
		return text;
	}

	/** Trim whitespace and normalize newline characters. */
	private static String normalizeText(String text) {
		return text.replace(SYS_LINE_SEP, "\n").trim();
	}

	public static String changeExtension(String filename, String newExtension) {
		int index = filename.lastIndexOf('.');
		if (index != -1) {
			return filename.substring(0, index) + newExtension;
		} else {
			return filename + newExtension;
		}
	}

//	@SuppressWarnings("javadoc")
//	public static Iterable<TestCase> getTestCases(File testDirectory, String extension) {
//		Collection<TestCase> tests = new ArrayList<>();
//		if (!testDirectory.isDirectory()) {
//			throw new Error("Could not find '" + testDirectory + "' directory!");
//		}
//		
//		for (File d: testDirectory.listFiles()) {
//			if (d.isDirectory()) {
//				TestCase c = new TestCase(d.getName());
//				for (File f : d.listFiles()) {
//					if (f.getName().endsWith(extension)) {
//						c.addSourceFile(f.getPath());
//					} else if (f.getName().endsWith(".conf")) {
//						c.setConfigFilePath(f.getPath());
//					}
//				}
//				tests.add(c);
//			}
//		}
//		return tests;
//	}

	public static Iterable<TestCase> getTestCases(File testDirectory) {
		Collection<TestCase> tests = new ArrayList<>();
		if (!testDirectory.isDirectory()) {
			throw new Error("Could not find '" + testDirectory + "' directory!");
		}
		
		for (File d: testDirectory.listFiles()) {
			if (d.isDirectory()) {
				TestCase c = new TestCase(d.getName());
				for (File f : d.listFiles()) {
					if (f.getName().endsWith(".conf")) {
						c.setConfigFilePath(f.getPath());
					}
				}
				tests.add(c);
			}
		}
		return tests;
	}
}