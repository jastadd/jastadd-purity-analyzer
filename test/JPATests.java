import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import jp.ac.osakau.farseerfc.purano.reflect.ClassFinder;
import jp.ac.osakau.farseerfc.purano.reflect.JPADumper;
import jp.ac.osakau.farseerfc.purano.util.JPAConfig;

@RunWith(Parameterized.class)
public class JPATests {
	/** Directory where the test input files are stored. */
	private static final File TEST_DIRECTORY = new File("test_files/");

	private TestCase testCase;

	public JPATests(TestCase testCase) {
		this.testCase = testCase;
	}

	@Test
	public void runTest() throws Exception {

		JPATestConfig tmp = new JPATestConfig();
		if (testCase.hasConfigFile()) {
			readConfigFile(tmp, testCase.getConfigFilePath());
		} else {
			tmp.ensureConfigIsComplete();
		}

    JPAConfig config = new JPAConfig(tmp.sideEffectMode(), new ArrayList<String>(), new ArrayList<String>(), new ArrayList<String>(), false, tmp.findCacheSemantics(), tmp.propagateToExternal());

    String targetPackage[] = { testCase.name };

    ClassFinder cf = new ClassFinder(Arrays.asList(targetPackage), config);
    cf.resolveMethods();

    JPADumper dd = new JPADumper(cf, "test_files/" + testCase.name + "/",
        config);

    dd.createMethodNodes();

    File actual = new File("test_files/" + testCase.name + "/actual");
    if (!actual.exists()) {
      actual.createNewFile();
    }

    boolean success = dd.dumpMethodNode("void " + testCase.name + ".A#f ()",
        "test_files/" + testCase.name + "/actual");
  	assertTrue("Failed to dump the method node", success);

		File expected = new File("test_files/" + testCase.name + "/expected");

		if (!expected.exists()) {
			System.out.println("Found no expected file for test case " + expected.getPath());
			System.out.println("Auto generating empty expected file");
			expected.createNewFile();
		}

		Util.compareOutput(actual, expected);
	}

	public void readConfigFile(JPATestConfig config, String configFilePath) {
		FileInputStream configFile = null;
		try {
			configFile = new FileInputStream(configFilePath);
			config.load(configFile);
		} catch (IOException e) {
		} finally {
			if (configFile != null) {
				try {
					configFile.close();
				} catch (IOException e) {
					// Should something be done here?
				}
			}
		}
		config.ensureConfigIsComplete();
	}

	@Parameters(name = "{0}")
	public static Iterable<TestCase> getTests() {
		return Util.getTestCases(TEST_DIRECTORY);
	}
}