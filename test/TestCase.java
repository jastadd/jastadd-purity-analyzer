import java.util.ArrayList;
import java.util.Collection;

public class TestCase {
	String name;
	String configFilePath;
	public TestCase(String name) {
		this.name = name;
		configFilePath = "";
	}
	
	public boolean hasConfigFile() {
		return !configFilePath.equals("");
	}

	public void setConfigFilePath(String configFile) {
		configFilePath = configFile;
	}
	
	public String getConfigFilePath() {
		return configFilePath;
	}
	
	@Override
	public String toString() {
		return name;
	}
}