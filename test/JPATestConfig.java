import java.util.Properties;

import jp.ac.osakau.farseerfc.purano.util.JPAConfig.SideEffectMode;

public class JPATestConfig extends Properties {
	public void ensureConfigIsComplete() {
		if (getProperty("sideEffectMode") == null) {
			setProperty("sideEffectMode", "default");
		}
		if (getProperty("findCacheSemantics") == null) {
			setProperty("findCacheSemantics", "true");
		}
		if (getProperty("propagateToExternal") == null) {
			setProperty("propagateToExternal", "false");
		}
	}
	
	public SideEffectMode sideEffectMode() {
		switch (getProperty("sideEffectMode")) {
		case "default":
			return SideEffectMode.DEFAULT;
		case "no_static":
			return SideEffectMode.NO_STATIC;
		case "static":
			return SideEffectMode.STATIC;
		case "native":
			return SideEffectMode.NATIVE;
		default:
			return SideEffectMode.DEFAULT;
		}
		
	}

	public boolean findCacheSemantics() {
		return Boolean.parseBoolean(getProperty("findCacheSemantics"));
	}

	public boolean propagateToExternal() {
		return Boolean.parseBoolean(getProperty("propagateToExternal"));
	}
}
